/*
 * $Id: Card.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 * A card is one member of a deck of playing cards.
 * @author greg
 */
public class Card implements Comparable {
    private Suit suit;
    private Rank rank;

    public Card() {

    }

    /**
     * Instantiate a Card from the abbreviation in a hand history file.
     * @param shortName Ex. Jh, 9c
     * @throws PokerException
     */
    public Card(String shortName) throws PokerException {
        if(shortName.length()!=2)
            throw new PokerException("Invalid Card Name");
        String tempRank = shortName.substring(0,1);
        String tempSuit = shortName.substring(1,2);
            suit=Suit.valueOfSuit(tempSuit);
            rank=Rank.valueOfRank(tempRank);
    }

    public Card(Rank rank, Suit suit) {
        this.suit=suit;
        this.rank=rank;
    }

    public Rank getRank() {
        return this.rank;
    }

    public Suit getSuit() {
        return this.suit;
    }

    /**
     * The card name based on the short names of the rank and suit.
     * Ex. Jh
     * @return {Short Rank}{Short Suit}
     */
    public String getShortName() {
        return rank.getShortName() + suit.getShortName();
    }

    /**
     * The card name based on the long names of the rank and suit.
     * Ex. Jack of Hearts
     * @return {Long Rank} of {Long Suit}
     */
    public String getLongName() {
        return rank.getLongName() + " of " + suit.getLongName();
    }

    public boolean isSuited(Card card) {
        return this.suit==card.suit;
    }

    public boolean isPaired(Card card) {
        return this.rank==card.rank;
    }

    /**
     * Returns true if these cards could be used together as parts of a straight.
     * @param card The card to pair with this card
     * @return true if connected
     */
    public boolean isConnected(Card card) {
        if(this.rank==card.rank) {
            return false;
        } else if(Math.abs(this.rank.ordinal()-card.rank.ordinal())<=4) {
            return true;
        } else if((this.rank==Rank.ACE && card.rank.ordinal()<=Rank.FIVE.ordinal())
                || (card.rank==Rank.ACE && this.rank.ordinal()<=Rank.FIVE.ordinal())) {
            return true;
        }
        return false;
    }

    public String toString() {
        return getLongName();
    }

    public int compareTo(Object t) {
        return ((Card)t).getRank().ordinal()-this.getRank().ordinal();
    }
}

