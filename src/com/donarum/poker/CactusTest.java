/*
 * $Id: CactusTest.java 2 2010-06-02 17:27:20Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 * I use this tester to see if my hand evaluation is correct.
 * For more info see:
 * http://www.suffecool.net/poker/evaluator.html
 * @author greg
 */
public class CactusTest {

    public static final void main(String[] args) {
        Deck deck = new Deck();
        Card[] cards = new Card[52];
        for(int i=0; i<cards.length; i++)
            cards[i] = deck.deal();

        int[] ranks = {0,0,0,0,0,0,0,0,0,0};
        int total = 0;

        for(int a=0; a<cards.length-4; a++) {
            for(int b=a+1; b<cards.length-3; b++) {
                for(int c=b+1; c<cards.length-2; c++) {
                    for(int d=c+1; d<cards.length-1; d++) {
                        for(int e=d+1; e<cards.length; e++) {
                            FiveCardHand hand = new FiveCardHand();
                            Card[] cc = new Card[5];
                            cc[0] = cards[a];
                            cc[1] = cards[b];
                            cc[2] = cards[c];
                            cc[3] = cards[d];
                            cc[4] = cards[e];
                            hand.setFiveCards(cc);
                            ranks[hand.getRank()]++;
                            total++;
                        }
                    }
                }
            }
        }
        for(int i=0; i<ranks.length; i++)
            System.out.println("" + i + " " + ranks[i]);
        System.out.println("total " + total);
    }

}

