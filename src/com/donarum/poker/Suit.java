/*
 * $Id: Suit.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 * Enumeration for suits.
 * @author greg
 */
public enum Suit {
    CLUBS("c","Clubs"),
    DIAMONDS("d","Diamonds"),
    HEARTS("h","Hearts"),
    SPADES("s","Spades");

    private final String shortName;
    private final String longName;

    Suit(String shortName, String longName) {
        this.shortName = shortName;
        this.longName = longName;
    }

    public String getShortName()   { return shortName; }
    public String getLongName() { return longName; }

    /**
     * Compare the given text to the name or abbreviation ignoring case and
     * return the match.
     * @param text Name or abbreviation of a suit.
     * @return One of the Suits
     * @throws PokerException if the value does not match anything.
     */
    public static Suit valueOfSuit(String text) throws PokerException {
        for(int i=0; i<Suit.values().length; i++) {
            if(text.equalsIgnoreCase(Suit.values()[i].getShortName()) || text.equalsIgnoreCase(Suit.values()[i].getLongName()))
                return Suit.values()[i];
        }
        throw new PokerException("Invalid suit");
    }

    public static void main(String[] args) {
        System.out.println(Suit.CLUBS);
        try {
            System.out.println(Suit.valueOfSuit("Hearts"));
            System.out.println(Suit.valueOfSuit("s"));
            System.out.println(Suit.valueOfSuit("D"));
            System.out.println(Suit.valueOfSuit("cLUbs"));
            System.out.println(Suit.valueOfSuit("greg"));
        } catch(PokerException ce) {
            System.out.println(ce.getMessage());
        }
    }

}

