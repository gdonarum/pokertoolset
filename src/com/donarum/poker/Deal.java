/*
 * $Id: Deal.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.util.Vector;

/**
 * A single instance of a game of poker, begun by shuffling the cards and ending
 * with the award of a pot. Also called a "hand" (though both terms are ambiguous).
 * @author greg
 */
public abstract class Deal {
    protected Vector<Seat> seats = new Vector<Seat>();

    public Deal() { }

    public void addPlayer(Player player, int seatNumber) {
        seats.add(new Seat(player,seatNumber));
    }

    protected int getNumSeats() {
        return this.seats.size();
    }

    protected void initiateSeatHand(int position, Hand hand) {
        this.seats.get(position).initiateHand(hand);
    }

    protected void dealSeatCard(int position, Card card) {
        this.seats.get(position).getHand().addCard(card);
    }

    public String toString() {
        String out = "";
//        Seat winner = null;
//        for(Seat seat : this.seats) {
//            out += seat.toString() + "\n";
//            if(winner==null) {
//                winner=seat;
//            } else {
//                switch(winner.getHand().compareTo(seat.getHand())) {
//                    case -1:
//                        winner=seat;
//                        break;
//                    case 0:
//                        System.out.println("Uh-Oh--tie");
//                }
//            }
//        }
//        out += "Winner = " + winner;
        return out;
    }


}

