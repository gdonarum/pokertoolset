/*
 * $Id: Deck.java 3 2010-06-02 17:27:51Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.util.Arrays;
import java.util.Collections;

/**
 * Standard deck
 * @author greg
 */
public class Deck {
    private Card[] cards = new Card[52];
    private int dealerIndex = 0;

    public Deck() {
        int counter = 0;
        for(Rank rank : Rank.values()) {
            for(Suit suit : Suit.values()) {
                cards[counter++] = new Card(rank, suit);
            }
        }
    }

    public void shuffle() {
        dealerIndex = 0;
        Collections.shuffle(Arrays.asList(cards));
    }

    public Card deal() {
        return cards[dealerIndex++];
    }

    public int getNumberOfCards() {
        return cards.length;
    }

    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();
        for(int i=0; i<deck.getNumberOfCards(); i++) {
            Card card = deck.deal();
            System.out.println(card.getLongName());
        }
    }
}

