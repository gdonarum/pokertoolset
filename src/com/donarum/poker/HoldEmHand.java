/*
 * $Id: HoldEmHand.java 16 2010-06-13 04:05:57Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 *
 * @author greg
 */
public class HoldEmHand extends FiveCardHand {

    private TexasHoldEmHoleCards holeCards;

    public void addCard(Card card) {
        super.addCard(card);
        if(cards.size()>=5)
            setBestFiveCardHand();
        if(cards.size()==2)
            holeCards = new TexasHoldEmHoleCards(cards.get(0),cards.get(1));
    }
    private void setBestFiveCardHand() {
        Hand hand = setBestFiveCardHand(null,this);
    }

    private Hand setBestFiveCardHand(Hand bestHand, Hand checkHand) {
        //System.out.println("CHECK: " + checkHand);
        if(checkHand.getNumberOfCards()==5) {
//            if(bestHand!=null) {
//                HoldEmHand h1 = (HoldEmHand)bestHand;
//                HoldEmHand h2 = (HoldEmHand)checkHand;
//                System.out.println(h1.getVerboseRank() + " " + h1.compareTo(h2) + " " + h2.getVerboseRank());
//            }
            if(bestHand==null || (bestHand.compareTo(checkHand)<0)) {
                bestHand = checkHand;
                Card[] list = new Card[5];
                for(int i=0; i<list.length; i++)
                    list[i] = bestHand.getCard(i);
                this.setFiveCards(list);
            }
        } else if (checkHand.getNumberOfCards()>5) {
            // remove one
            for(int i=0; i<checkHand.getNumberOfCards(); i++) {
                HoldEmHand newHand = new HoldEmHand();
                for(int c=0; c<checkHand.getNumberOfCards(); c++) {
                    if(c!=i)
                        newHand.addCard(checkHand.getCard(c));
                }
                bestHand = setBestFiveCardHand(bestHand, newHand);
            }
        }
        return bestHand;
    }

    public boolean isSuited() {
        if (this.cards.size() >= 2) {
            return this.cards.get(0).isSuited(this.cards.get(1));
        }
        return false;
    }

    public boolean isPaired() {
        if (this.cards.size() >= 2) {
            return this.cards.get(0).isPaired(this.cards.get(1));
        }
        return false;
    }

    public boolean isConnected() {
        if (this.cards.size() >= 2) {
            return this.cards.get(0).isConnected(this.cards.get(1));
        }
        return false;
    }

    public TexasHoldEmHoleCards getHoleCards() {
        return this.holeCards;
    }

//    public String getClassification() {
//        String out = "";
//        if(cards.get(0).getRank().compareTo(cards.get(1).getRank())>=0)
//            out += cards.get(0).getRank().getShortName() + cards.get(1).getRank().getShortName();
//        else
//            out += cards.get(1).getRank().getShortName() + cards.get(0).getRank().getShortName();
//        if(isPaired())
//            out += "";
//        else if(isSuited())
//            out +="s";
//        else
//            out += "o";
//        return out;
//    }
//
//    /**
//     * http://www.toptexasholdem.com/texas-holdem-strategy-startinghand.php
//     * @return
//     */
//    public Range getRange() {
//        if("AA,KK,QQ,AKs".contains(this.getClassification()))
//            return Range.A;
//        if("JJ,TT,AKo,AQs".contains(this.getClassification()))
//            return Range.B;
//        if("99,AQo,AJs,KQs,KQo".contains(this.getClassification()))
//            return Range.C;
//        if("88,77,66,55,44,33,22,KJs,KTs,QJs,QTs,JTs,T9s,98s,87s,76s".contains(this.getClassification()))
//            return Range.D;
//        if(this.getClassification().startsWith("A") && this.getClassification().endsWith("s"))
//            return Range.D;
//        return Range.E;
//    }
//
//    /**
//     * http://en.wikipedia.org/wiki/Chen_formula#Chen_formula
//     * Chen is the author of the Chen Formula which was published in Lou Krieger's
//     * 2000 book Hold 'em Excellence. The formula ranks Texas hold 'em starting
//     * hands by assigning them a numerical value, as follows:
//     * 1. Take the high card and score it. A=10,K=8,Q=7,J=6 and 10 to 2 = 1/2 the
//     * value of the card. (a 6 is worth 3, for example)
//     * 2. If the 2nd card pairs the first, then the value is twice the high card
//     * point. The minimum score for a pair is 5 (e.g., pair of deuces is worth 5, not 4).
//     * 3. Round up any half points.
//     * 4. If the cards are not paired then calculate the gap for the lower card
//     * and subtract a gap penalty. The gap is the number of cards required to
//     * complete the sequence, for example, a 9 and 6 have a gap of 2, needing an 8 and 7 to complete the sequence:
//          * For a gap of 0 subtract 0.
//          * For a gap of 1 subtract 1.
//          * For a gap of 2 subtract 2.
//          * For a gap of 3 subtract 4.
//          * For a gap of 4 or more subtract 5 (includes A2,A3,A4, A5).
//     * 5. If the cards are of the same suit apply a flush bonus of +2 pts.
//     * 6. If the cards have a gap of 0 or 1 and the top card is a J or lower apply a +1 straight bonus.
//     * @return
//     */
//    public int getChenScore() {
//        int score = 0;
//        // 1. score high card
//        float fScore=0;
//        switch(cards.get(0).getRank()) {
//            case ACE:
//                fScore=10;
//                break;
//            case KING:
//                fScore=8;
//                break;
//            case QUEEN:
//                fScore=7;
//                break;
//            case JACK:
//                fScore=6;
//                break;
//            default:
//                fScore = (float)(cards.get(0).getRank().ordinal()+2)/(float)2;
//        }
//
//        // 2. pairs
//        boolean isPair = cards.get(0).getRank()==cards.get(1).getRank();
//        if(isPair) {
//            fScore=fScore*2;
//            if(fScore==3) fScore=4;
//            else if(fScore<5) fScore=5;
//        }
//
//        // 3. round up
//        score = Math.round(fScore);
//
//        // 4. gap
//        int gap = cards.get(0).getRank().ordinal()-cards.get(1).getRank().ordinal()-1;
//        if(gap<0) gap=0;
//        if(gap>5) gap=5;
//        score=score-gap;
//
//        // 5. flush bonus
//        if(cards.get(0).getSuit()==cards.get(1).getSuit())
//            score+=2;
//
//        // 6. If the cards have a gap of 0 or 1 and the top card is a J or lower apply a +1 straight bonus.
//        if(gap<2 && !isPair) {
//            if(cards.get(0).getRank().ordinal()<10)
//                score++;
//        }
//
//        return score;
//    }

    public String toString() {
        return super.toString() + "," + this.isSuited() + "," + this.isConnected() + "," + this.isPaired();
    }
}

