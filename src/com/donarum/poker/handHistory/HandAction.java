/*
 * $Id: HandAction.java 17 2010-06-14 03:33:46Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

/**
 *
 * @author greg
 */
public class HandAction {

    public static enum Round {PRE_FLOP, FLOP, TURN, RIVER};
    public static enum Action {POST_SB, POST_BB, POST_SB_PENALTY, POST_BB_PENALTY, CALL, BET, RAISE, CHECK, FOLD, COLLECTED, UNCALLED_BET_RETURNED};

    private Round round;
    private Action action;
    private int value;
    private HoldEmHand hand;
    private int currentPotSize;
    private int currentCallAmount;

    /**
     * Constuct a new action object
     * @param round the stage of the deal (preflop, flop, turn, river)
     * @param action the type of action (call, raise, fold, etc.)
     * @param value the value of the action (ex. calls 4 cents)
     * @param playerName the player taking the action
     * @param currentPotSize the amount in the pot
     * @param currentCallAmount the amount that this player must call to continue
     */
    public HandAction(Round round, Action action, int value, HoldEmHand hand, int currentPotSize, int currentCallAmount) {
        this.round=round;
        this.action=action;
        this.value=value;
        this.hand=hand;
        this.currentPotSize=currentPotSize;
        this.currentCallAmount=currentCallAmount;
    }

    /**
     * @return the round
     */
    public Round getRound() {
        return round;
    }

    /**
     * @return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @return the playerName
     */
    public HoldEmHand getHoldEmHand() {
        return hand;
    }

    /**
     * @return the currentPotSize
     */
    public int getCurrentPotSize() {
        return currentPotSize;
    }

    /**
     * @return the currentCallAmount
     */
    public int getCurrentCallAmount() {
        return currentCallAmount;
    }

    public String toString() {
        String out = "round: " + getRound() + "\n";
        out += "playerName: " + getHoldEmHand().getPlayerName() + "\n";
        out += "action: " + getAction() + "\n";
        out += "value: " + getValue() + "\n";
        out += "currentPotSize: " + getCurrentPotSize() + "\n";
        out += "currentCallAmount: " + getCurrentCallAmount() + "\n";
        return out;
    }
}
