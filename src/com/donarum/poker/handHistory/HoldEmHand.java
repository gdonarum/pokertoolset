/*
 * $Id: HoldEmHand.java 17 2010-06-14 03:33:46Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

import com.donarum.poker.Card;
import com.donarum.poker.HoldEmHoleCards;
import com.donarum.poker.TexasHoldEmHoleCards;
import com.donarum.poker.handHistory.HandAction.Action;
import com.donarum.poker.handHistory.HandAction.Round;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class HoldEmHand {

    private int id = -1;
    private String playerName = "";
    private int seatNumber = -1;
    private boolean button = false;
    private HoldEmHoleCards holeCards;
    private HoldEmDeal deal;
    private Vector<HandAction> myActions = new Vector<HandAction>();

    public HoldEmHand(HoldEmDeal deal) {
        this.deal=deal;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the dealId
     */
    public int getDealId() {
        return this.deal.getId();
    }

    /**
     * @return the playerName
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * @param playerName the playerName to set
     */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    /**
     * @return the seatNumber
     */
    public int getSeatNumber() {
        return seatNumber;
    }

    /**
     * @param seatNumber the seatNumber to set
     */
    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    /**
     * @return the button
     */
    public boolean isButton() {
        return button;
    }

    /**
     * @param button the button to set
     */
    public void setButton(boolean button) {
        this.button = button;
    }

    /**
     * @return the smallBlind
     */
    public boolean isSmallBlind() {
        for(HandAction a : getMyActions()) {
            if(a.getAction()==Action.POST_SB)
                return true;
        }
        return false;
    }

    /**
     * @return the bigBlind
     */
    public boolean isBigBlind() {
        for(HandAction a : getMyActions()) {
            if(a.getAction()==Action.POST_BB)
                return true;
        }
        return false;
    }

    /**
     * @return true if this player voluntarily added money to the pot preflop
     */
    public boolean isVpip() {
        for(HandAction a : getMyActions()) {
            if(a.getRound()==Round.PRE_FLOP) {
                if(a.getAction()==Action.BET
                        || a.getAction()==Action.CALL
                        || a.getAction()==Action.RAISE)
                    return true;
            }
        }
        return false;
    }

    /**
     * @return true if this player raised preflop
     */
    public boolean isPreFlopRaise() {
        for(HandAction a : getMyActions()) {
            if(a.getRound()==Round.PRE_FLOP) {
                if(a.getAction()==Action.BET
                        || a.getAction()==Action.RAISE)
                    return true;
            }
        }
        return false;
    }

    /**
     * @return the positive profit or negative loss in cents
     */
    public int getProfit() {
        int profit = 0;
        Round alreadyRaisedRound = null;
        for(int i=getMyActions().size()-1; i>=0; i--) {
            HandAction action = getMyActions().get(i);
            // the last raise in a round accounts for the entire bet size
            // When the player pays a small blind penalty it does not count toward
            //  his portion of the bet and thus must always be counted
            if(action.getRound()!=alreadyRaisedRound || action.getAction()==Action.POST_SB_PENALTY) {
                profit += action.getValue();
                if(action.getAction()==Action.RAISE)
                    alreadyRaisedRound=action.getRound();
            }
        }
        return profit;
    }

    /**
     * @return true if the player won or tied the hand
     */
    public boolean isWinner() {
        return (getMyActions().size()>0) && (getMyActions().get(getMyActions().size()-1).getAction() == Action.COLLECTED);
    }

    /**
     * Used as part of aggression factor
     * @return total number of times the player opens a round of betting
     */
    public int getNumberOfBets() {
        int out = 0;
        for(HandAction a : this.getMyActions()) {
            if(a.getAction()==Action.BET)
                out++;
        }
        return out;
    }

    /**
     * Used as part of aggression factor
     * @return total number of times the player raises a bet
     */
    public int getNumberOfRaises() {
        int out = 0;
        for(HandAction a : this.getMyActions()) {
            if(a.getAction()==Action.RAISE)
                out++;
        }
        return out;
    }

    /**
     * Used as part of aggression factor
     * @return total number of times the player calls a bet
     */
    public int getNumberOfCalls() {
        int out = 0;
        for(HandAction a : this.getMyActions()) {
            if(a.getAction()==Action.CALL)
                out++;
        }
        return out;
    }

    /**
     * @return the holeCards
     */
    public HoldEmHoleCards getHoleCards() {
        return holeCards;
    }

    /**
     * @param holeCards the holeCards to set
     */
    public void setHoleCards(HoldEmHoleCards holeCards) {
        this.holeCards = holeCards;
    }

    /**
     * @return the HandActions
     */
    public Vector<HandAction> getMyActions() {
        return myActions;
    }

    /**
     * @param HandAction add an action
     */
    public synchronized void addAction(HandAction.Round round, HandAction.Action action, int value) {
        HandAction ha = new HandAction(round, action, value, this, deal.getCurrentPotSize(), deal.getCurrentBetSize()-this.getCurrentBetSize());
        this.deal.getActions().add(ha);
        this.myActions.add(ha);
    }

    /**
     * Calculates the current bet size for this player.  This is the amount
     * that this player has put into the pot this round.
     * @return
     */
    public int getCurrentBetSize() {
        int out = 0;
        for(int i=this.getMyActions().size()-1; i>=0; i--) {
            HandAction a = this.getMyActions().get(i);
            if(a.getRound()!=this.deal.getActions().lastElement().getRound())
                return out;
            if(a.getAction()==Action.BET || a.getAction()==Action.RAISE || a.getAction()==Action.POST_BB || a.getAction()==Action.POST_SB)
                return out + a.getValue();
            if(a.getAction()==Action.CALL || a.getAction()==Action.POST_BB_PENALTY)
                out += a.getValue();
        }
        return out;
    }

    public String toString() {
        String out = "";
        out += "dealId: " + getDealId() + "\n";
        out += "playerName: " + playerName + "\n";
        out += "seatNumber: " + seatNumber + "\n";
        out += "profit: " + getProfit() + "\n";
        out += "winner: " + isWinner() + "\n";
        out += "button: " + button + "\n";
        out += "smallBlind: " + isSmallBlind() + "\n";
        out += "bigBlind: " + isBigBlind() + "\n";
        out += "vpip: " + isVpip() + "\n";
        out += "preFlopRaise: " + isPreFlopRaise() + "\n";
        out += "bets: " + getNumberOfBets() + "\n";
        out += "raises: " + getNumberOfRaises() + "\n";
        out += "calls: " + getNumberOfCalls() + "\n";
        out += "holeCards: " + holeCards + "\n";
        return out;
    }


}
