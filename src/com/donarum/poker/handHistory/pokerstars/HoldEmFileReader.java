/*
 * $Id: HoldEmFileReader.java 17 2010-06-14 03:33:46Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory.pokerstars;

import com.donarum.poker.Card;
import com.donarum.poker.HoldEmHoleCards;
import com.donarum.poker.OmahaHoleCards;
import com.donarum.poker.TexasHoldEmHoleCards;
import com.donarum.poker.Player;
import com.donarum.poker.PokerException;
import com.donarum.poker.handHistory.HandAction;
import com.donarum.poker.handHistory.HandAction.Action;
import com.donarum.poker.handHistory.HandAction.Round;
import com.donarum.poker.handHistory.HandHistoryException;
import com.donarum.poker.handHistory.HandHistoryFileReader;
import com.donarum.poker.handHistory.HistoryStorage;
import com.donarum.poker.handHistory.HoldEmDeal;
import com.donarum.poker.handHistory.HoldEmHand;
import com.donarum.poker.handHistory.MySQLHistoryStorage;
import com.donarum.poker.handHistory.ParsingLibrary;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class HoldEmFileReader implements HandHistoryFileReader {

    private HistoryStorage storage;

    private enum Stage {HEADER, PRE_FLOP, FLOP, TURN, RIVER, SHOW_DOWN, SUMMARY, END_OF_HAND};

    // globals
    private String currentLine;
    private HoldEmDeal deal = null;
    private Stage stage = Stage.HEADER;
    private String buttonSeat = "";
    private boolean chipLine = false;
    private boolean foundBigBlind = false; // sometimes multiple people must post the bb
    private int smallBlind = 0;

    public HoldEmFileReader() {}

    @Override
    public boolean canRead(File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file.getAbsolutePath()));
            String firstLine = "";
            while ((firstLine = br.readLine()) != null) {
                if(firstLine.length()>0) {
                    if(ParsingLibrary.matches(".*PokerStars Game #[0-9]*:  Omaha Pot Limit \\(\\$[0-9\\.]*/\\$[0-9\\.]*.*", firstLine))
                        return true;
                    if(ParsingLibrary.matches(".*PokerStars Game #[0-9]*:  Hold'em Limit \\(\\$[0-9\\.]*/\\$[0-9\\.]*.*", firstLine))
                        return true;
                    if(ParsingLibrary.matches(".*PokerStars Game #[0-9]*:  Hold'em No Limit \\(\\$[0-9\\.]*/\\$[0-9\\.]*.*", firstLine))
                        return true;
                    if(ParsingLibrary.matches(".*PokerStars Game #[0-9]*:  Omaha Limit \\(\\$[0-9\\.]*/\\$[0-9\\.]*.*", firstLine))
                        return true;
                    return false;
                }
            }
        } catch (FileNotFoundException fnfe) {
            
        } catch(IOException ioe) {
            
        }

        return false;
    }

    @Override
    public void readFile(File file) throws HandHistoryException {
        readFile(file.getAbsolutePath());
    }

    public void setStorage(HistoryStorage storage) {
        this.storage=storage;
    }

    public void readFile(String fileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));

            while ((currentLine = br.readLine()) != null) {
                ///////////////////////
                // STAGES
                ///////////////////////
                if(currentLine.startsWith("*** HOLE CARDS ***")) {
                    stage = Stage.PRE_FLOP;
                }
                else if(currentLine.startsWith("*** FLOP ***")) {
                    stage = Stage.FLOP;
                }
                else if(currentLine.startsWith("*** TURN ***")) {
                    stage = Stage.TURN;
                }
                else if(currentLine.startsWith("*** RIVER ***")) {
                    stage = Stage.RIVER;
                }
                else if(currentLine.startsWith("*** SHOW DOWN ***")) {
                    stage = Stage.SHOW_DOWN;
                }
                else if(currentLine.startsWith("*** SUMMARY ***")) {
                    stage = Stage.SUMMARY;
                }
                else if(currentLine.equals("")) {
                    stage = Stage.END_OF_HAND;
                }

                ///////////////////////
                // CHIP ACTION
                ///////////////////////
                chipLine = handleChipAction(); //currentLine, deal, stage);

                ///////////////////////
                // ANY STAGE
                ///////////////////////
                if(currentLine.contains(" joins the table at seat")) {
                    // do nothing
                }
                else if(currentLine.endsWith(" leaves the table")) {
                    // do nothing
                }
                else if(currentLine.contains(" is disconnected")) {
                    // do nothing
                }
                else if(currentLine.contains(" is connected")) {
                    // do nothing
                }
                else if(currentLine.contains(" has timed out")) {
                    // do nothing
                }
                else if(currentLine.contains(" said, \"")) {
                    // do nothing
                }
                else if(currentLine.contains(": shows ") || currentLine.contains(": mucked ")) {
                    try {
                        String sCards = ParsingLibrary.sedLikeMatcher("\\[(.*)\\]", currentLine);
                        String playerName = ParsingLibrary.sedLikeMatcher("^(.*):", currentLine);
                        HoldEmHand h = deal.getHandByPlayerName(playerName);
                        h.setHoleCards(handleHoleCards(sCards));
                    } catch(PokerException pe) {
                        logError(pe, currentLine);
                    }
                }
                ///////////////////////
                // HEADER
                ///////////////////////
                // first line of deal
                else if(currentLine.contains("PokerStars Game")) {
//                    System.out.println(currentLine);
                    // close out the last table if there was one
                    if(deal!=null)
                        endOfDeal(deal);
                    // init
                    stage=Stage.HEADER;
                    buttonSeat = "";
                    deal = new HoldEmDeal();
                    foundBigBlind = false;
                    deal.setSite("PS");
                    // read
                    String[] values = ParsingLibrary.sedLikeMultiMatcher("PokerStars Game #([0-9]*):  (.*) \\(\\$([0-9\\.]*)/\\$([0-9\\.]*).*([0-9][0-9][0-9][0-9]/.*:[0-9][0-9])", currentLine);
                    if(values.length==5) {
                        deal.setGameNumber(values[0]);
                        // game type
                        deal.setGameType(values[1].replaceAll("'", ""));
//                        // NL shows blinds, limit shows bets
//                        values[2];
//                        values[3];
                        // time stamp
                        deal.setTimestamp(values[4].replaceAll("/", "-"));
                    }
                    // description
                    deal.setDescription(currentLine);
                }
                // second line of deal
                else if(currentLine.startsWith("Table '")) {
                    // table name
                    String tableName = ParsingLibrary.sedLikeMatcher("'(.*)'", currentLine);
                    deal.setTableName(tableName);
                    deal.setDescription(deal.getDescription() + "; " + currentLine);
                    // button
                    buttonSeat = ParsingLibrary.sedLikeMatcher("#([0-9]*) ", currentLine);
                }
                // seat list
                else if(stage==Stage.HEADER && currentLine.startsWith("Seat ")) {
                    String seatNum = ParsingLibrary.sedLikeMatcher("Seat (.*):", currentLine);
                    String playerName = ParsingLibrary.sedLikeMatcher("Seat [0-9*]*: (.*) [\\(]", currentLine);
                    HoldEmHand hand = new HoldEmHand(deal);
                    hand.setPlayerName(playerName);
                    hand.setSeatNumber(Integer.parseInt(seatNum));
                    hand.setButton(seatNum.equals(buttonSeat));
                    deal.addHand(hand);
                }
                ///////////////////////
                // PRE FLOP
                ///////////////////////
                else if(stage==Stage.PRE_FLOP && currentLine.startsWith("Dealt to ")) {
                    try {
                        String sCards = ParsingLibrary.sedLikeMatcher("\\[(.*)\\]", currentLine);
                        String playerName = ParsingLibrary.sedLikeMatcher("Dealt to (.*) \\[", currentLine);
                        HoldEmHand h = deal.getHandByPlayerName(playerName);
                        h.setHoleCards(handleHoleCards(sCards));
                    } catch(PokerException pe) {
                        logError(pe, currentLine);
                    }
                }
                ///////////////////////
                // FLOP
                ///////////////////////
                else if(currentLine.startsWith("*** FLOP ***")) {
                    String flop = ParsingLibrary.sedLikeMatcher("\\[(.*)\\]", currentLine);
                }
                ///////////////////////
                // TURN
                ///////////////////////
                else if(currentLine.startsWith("*** TURN ***")) {
                    String turn = ParsingLibrary.sedLikeMatcher(".*\\[(.*)\\]", currentLine);
                }
                ///////////////////////
                // RIVER
                ///////////////////////
                else if(currentLine.startsWith("*** RIVER ***")) {
                    String river = ParsingLibrary.sedLikeMatcher(".*\\[(.*)\\]", currentLine);
                }
                ///////////////////////
                // SHOW DOWN
                ///////////////////////
//                else if(stage==Stage.SHOW_DOWN) {
//
//                }
                ///////////////////////
                // SUMMARY
                ///////////////////////
                else if(stage==Stage.SUMMARY && currentLine.startsWith("Total pot ")) {
                    String pot = ParsingLibrary.sedLikeMatcher("Total pot \\$([0-9\\.]*)", currentLine);
                    deal.setPot(parseMoney(pot));
                    String rake = ParsingLibrary.sedLikeMatcher(" \\| Rake \\$([0-9\\.]*)", currentLine);
                    deal.setRake(parseMoney(rake));
                }
                else {
//                    System.out.println("UNHANDLED LINE: " + currentLine);
                }
            }
            if(deal!=null)
                endOfDeal(deal);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HoldEmHoleCards handleHoleCards(String cards) throws PokerException {
        String[] split = cards.split(" ");
        if(split.length==2) {
            Card card1 = new Card(split[0]);
            Card card2 = new Card(split[1]);
            return new TexasHoldEmHoleCards(card1, card2);
        }
        if(split.length==4) {
            Card card1 = new Card(split[0]);
            Card card2 = new Card(split[1]);
            Card card3 = new Card(split[2]);
            Card card4 = new Card(split[3]);
            return new OmahaHoleCards(card1, card2, card3, card4);
        }
        throw new PokerException("Invalid hole cards.");
    }

    private boolean handleChipAction() { //String line, HoldEmDeal deal, Stage stage) {
        // set the round
        Round round = Round.PRE_FLOP;
        switch(stage) {
            case HEADER:
            case PRE_FLOP:
                round = Round.PRE_FLOP;
                break;
            case FLOP:
                round = Round.FLOP;
                break;
            case TURN:
                round = Round.TURN;
                break;
            case RIVER:
            case SHOW_DOWN:
            case SUMMARY:
            case END_OF_HAND:
                round = Round.RIVER;
                break;
        }

        // set up queries
        String[] regExs = new String[10];
        // out
        regExs[0] = "^(.*): posts small blind \\$([0-9\\.]*)";
        regExs[1] = "^(.*): posts big blind \\$([0-9\\.]*)";
        regExs[2] = "^(.*): calls \\$([0-9\\.]*)";
        regExs[3] = "^(.*): bets \\$([0-9\\.]*)";
        regExs[4] = "^(.*): raises \\$[0-9\\.]* to \\$([0-9\\.]*)";
        regExs[5] = "^(.*): posts small & big blinds \\$([0-9\\.]*)";
        // fold / checks
        regExs[6] = "^(.*): folds";
        regExs[7] = "^(.*): checks";
        // in
        regExs[8] = "^(.*) collected \\$([0-9\\.]*)";
        regExs[9] = "^Uncalled bet \\(\\$([0-9\\.]*)\\) returned to (.*)";

        for(int i=0; i<regExs.length; i++) {
            String[] out = ParsingLibrary.sedLikeMultiMatcher(regExs[i], currentLine);
            if(out.length==2 && out[0]!=null && out[1]!=null) {
                int multiplier = -1;
                Action action = Action.CHECK;
                switch (i) {
                    case 0:
                        if(!foundBigBlind) {
                            action = Action.POST_SB;
                        } else {
                            action = Action.POST_SB_PENALTY;
                        }
                        break;
                    case 1:
                        if(!foundBigBlind) {
                            action = Action.POST_BB;
                            foundBigBlind=true;
                        } else {
                            action = Action.POST_BB_PENALTY;
                        }
                        break;
                    case 2:
                        action = Action.CALL;
                        break;
                    case 3:
                        action = Action.BET;
                        break;
                    case 4:
                        action = Action.RAISE;
                        break;
                    case 5:
                        action = Action.POST_SB_PENALTY;
                        break;
                    case 8:
                        multiplier=1;
                        action = Action.COLLECTED;
                        break;
                    case 9:
                        String tmp = out[0];
                        out [0] = out[1];
                        out[1] = tmp;
                        multiplier=1;
                        action = Action.UNCALLED_BET_RETURNED;
                        break;
                }
                // add the action
                try {
                    HoldEmHand h = deal.getHandByPlayerName(out[0]);
                    int money = parseMoney(out[1]);
                    if(action==Action.POST_SB_PENALTY && money!=smallBlind) {
                        // This is the case where the player posts both the small and big blinds.
                        // Only the big blind counts towards his share of the betting.
                        h.addAction(round, action, (multiplier*smallBlind));
                        h.addAction(round, Action.POST_BB_PENALTY, (multiplier*(money-smallBlind)));
                    } else {
                        h.addAction(round, action, (multiplier*money));
                    }
                    if(action==Action.POST_SB) {
                        smallBlind = money;
                        deal.setSmallBlind(money);
                    } else if(action==Action.POST_BB) {
                        deal.setBigBlind(money);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
            // folds and checks
            if(out.length==1 && out[0]!=null) {
                Action action = Action.CHECK;
                switch (i) {
                    case 6:
                        action = Action.FOLD;
                        break;
                    case 7:
                        action = Action.CHECK;
                        break;
                }
                // add the action
                try {
                    HoldEmHand h = deal.getHandByPlayerName(out[0]);
                    h.addAction(round, action, 0);
                } catch(Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        }
        return false;
    }

    private void endOfDeal(HoldEmDeal deal) {
        if(isValidDeal(deal)) {
            int dealId = storage.storeHoldEmDeal(deal);
            deal.setId(dealId);
            // store players to db
            for(HoldEmHand hand : deal.getHands()) {
                int id = storage.storeHoldEmHand(hand);
                hand.setId(id);
            }
            // store players to db
            for(HandAction action : deal.getActions()) {
                int id = storage.storeHandAction(action);
            }
    //        // print
    //        System.out.println();
    //        System.out.println();
    //        System.out.println(deal);
        } else {
            System.out.println("ERROR: Bad deal - " + deal.getDescription());
        }
    }

    private boolean isValidDeal(HoldEmDeal deal) {
        // hands add up
        int profit = 0;
        for(HoldEmHand hand : deal.getHands()) {
            profit += hand.getProfit();
        }
        boolean handsAddUp = deal.getRake()+profit==0;
        // actions add up
        int collected = 0;
        for(int i=deal.getActions().size()-1; i>=0; i--) {
            if(deal.getActions().get(i).getAction()==Action.COLLECTED) {
                collected += deal.getActions().get(i).getValue();
            }
        }
        boolean actionsAddUp = (deal.getPot()-deal.getRake()-collected==0);
        // return
        return (handsAddUp && actionsAddUp);
    }

    private int parseMoney(String money) {
        // do not use double math because of rounding errors
        int out = 0;
        String m = money;
        // handle cents
        if(m.contains(".")) {
            m = m.replace(".", "");
        } else {
            m+="00";
        }
        try {
            out = Integer.parseInt(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    private void logError(Exception error, String currentLine) {
        System.out.println("ERROR: " + error.getMessage() + " {" + currentLine + "}");
    }

    public static final void main(String[] args) {
        HoldEmFileReader reader = new HoldEmFileReader();
        reader.setStorage(new MySQLHistoryStorage());
//        reader.readFile("/home/greg/Desktop/badActionValues.txt");
//        reader.readFile("/home/greg/Desktop/bad.txt");
//        reader.readFile("/home/greg/Desktop/goodExample");
//        reader.readFile("/home/greg/Desktop/HH20100417 Isergina - $0.01-$0.02 - USD No Limit Hold'em.txt");
        reader.readFile("/home/greg/Desktop/GD73/HH20100417 Tubingia fast - $0.01-$0.02 - USD No Limit Hold'em.txt");

//        File dir = new File("/home/greg/Desktop/GD73");
//        FilenameFilter filter = new FilenameFilter() {
//
//            public boolean accept(File dir, String name) {
//                return name.contains("Hold'em") && !name.contains("+"); // the plus is for tournaments
//            }
//        };
//        File[] files = dir.listFiles(filter);
//        for(File f : files) {
//            reader.readFile(f.getAbsolutePath());
//        }
    }
}
