/*
 * $Id: HandHistoryImporter.java 16 2010-06-13 04:05:57Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author greg
 */
public class HandHistoryImporter {
    private Vector<HandHistoryFileReader> readers = new Vector<HandHistoryFileReader>();
    private HistoryStorage storage;

    public HandHistoryImporter(HistoryStorage storage) {
        this.storage=storage;
        // in the future it will get the list of file readers from a config file,
        //  but for now hard code them
        HandHistoryFileReader pshe = new com.donarum.poker.handHistory.pokerstars.HoldEmFileReader();
        pshe.setStorage(storage);
        addFileReader(pshe);
    }

    public void addFileReader(HandHistoryFileReader reader) {
        this.readers.add(reader);
    }

    public void readFile(File file) throws HandHistoryException {
        for(HandHistoryFileReader reader : readers) {
            if(reader.canRead(file)) {
                reader.readFile(file);
                return;
            }
        }
        // throw exception if we do not have a reader that applies
        throw new HandHistoryException("No applicable reader found: " + file.getName());
    }

    public static final void main(String[] args) {
        HandHistoryImporter importer = new HandHistoryImporter(new MySQLHistoryStorage());

        File dir = new File("/home/greg/Desktop/GD73");
        FilenameFilter filter = new FilenameFilter() {

            public boolean accept(File dir, String name) {
                return !name.contains("+"); // the plus is for tournaments
            }
        };
        File[] files = dir.listFiles(filter);
        for(File f : files) {
            try {
                importer.readFile(f);
            } catch (HandHistoryException ex) {
                System.out.println(ex.getMessage());
//                Logger.getLogger(HandHistoryImporter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
