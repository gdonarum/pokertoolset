/*
 * $Id: ParsingLibrary.java 16 2010-06-13 04:05:57Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author greg
 */
public class ParsingLibrary {

    public static boolean matches(String regex, String text) {
        try {
            Matcher m = Pattern.compile(regex).matcher(text);
            m.find();
            return m.matches();
        } catch(Exception e) {

        }
        return false;
    }

    public static String sedLikeMatcher(String regex, String text) {
        try {
            Matcher m = Pattern.compile(regex).matcher(text);
            m.find();
            return m.group(1);
        } catch(Exception e) {

        }
        return "";
    }

    public static String[] sedLikeMultiMatcher(String regex, String text) {
        String[] out = new String[0];
        try {
            Matcher m = Pattern.compile(regex).matcher(text);
            m.find();
            out = new String[m.groupCount()];
            for(int i=0; i<out.length; i++)
                out[i] = m.group(i+1);
        } catch(Exception e) {

        }
        return out;
    }

    public static final void main(String[] args) {
//        String html = "<html>" +
//            "junk which you don't want" +
//            "<body attributes='and stuff'>" +
//            "stuff you want" +
//            "</body>" +
//            "</html>";
//
//        String out = sedLikeMatcher("<body[^>]*>(.*)</body>", html);
//        System.out.println(out);
        
        
//        String text = "Seat 4: Serj S-V ($1.96 in chips)";
//
//        String out = sedLikeMatcher("Seat [0-9*]: (.*) [\\(]", text);
//        System.out.println(out);
        
//        String text = "PokerStars Game #33090627532:  Hold'em No Limit ($0.01/$0.02 USD) - 2009/09/20 22:12:53 ET";
//
//        String out = sedLikeMatcher("\\$([0-9\\.]*)/", text);
//        System.out.println(out);

//        String text = "Table 'Agnia' 6-max Seat #4 is the button";
//
//        String out = sedLikeMatcher("#([0-9]*) ", text);
//        System.out.println(out);

//        String text = "alex_lobanof: posts small blind $0.01";
//
//        String out = sedLikeMatcher("^(.*):", text);
//        out = sedLikeMatcher("\\$(.*)", text);
//        System.out.println(out);

//        String text = "Dealt to GD 73 [Kc 3s]";
//
//        String out = sedLikeMatcher("Dealt to (.*) \\[", text);
//        System.out.println(out);

//        String text = "*** TURN *** [Jh Ts 6c] [Qc]";
//
//        String out = sedLikeMatcher(".*\\[(.*)\\]", text);
//        System.out.println(out);

//        String text = "MR_BON_BEAT: bets $0.40";
//
//        String[] out = sedLikeMultiMatcher("^(.*): bets \\$([0-9\\.]*)", text);
//        for(String o : out)
//           System.out.println(o);

////        String text = "PokerStars Game #42820465569:  Hold'em No Limit ($0.01/$0.02 USD) - 2010/04/17 21:30:49 ET";
//        String text = "PokerStars Game #43059537124:  Hold'em Limit ($0.02/$0.04 USD) - 2010/04/22 23:34:02 ET";
//
////        String[] out = sedLikeMultiMatcher("PokerStars Game #([0-9]*):  (.*) \\(\\$([0-9\\.]*)/\\$([0-9\\.]*) - (.*)", text);
//        String[] out = sedLikeMultiMatcher("PokerStars Game #([0-9]*):  (.*) \\(\\$([0-9\\.]*)/\\$([0-9\\.]*).*([0-9][0-9][0-9][0-9]/[0-9][0-9]/[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9])", text);
//        for(String o : out)
//           System.out.println(o);

        String text = "PokerStars Game #42914718741:  Omaha Pot Limit ($0.01/$0.02 USD) - 2010/04/19 20:42:05 ET";
        String reg =  "PokerStars Game #[0-9]*:  Omaha Pot Limit \\(\\$[0-9\\.]*/\\$[0-9\\.]*.*";
//        String reg = "PokerStars Game #[0-9]*:  Omaha Pot Limit .*";
        if(matches(reg,text))
            System.out.println("match");
        else
            System.out.println("no match");


    }
}
