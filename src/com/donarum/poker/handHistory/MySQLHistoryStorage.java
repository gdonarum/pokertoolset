/*
 * $Id: MySQLHistoryStorage.java 17 2010-06-14 03:33:46Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author greg
 */
public class MySQLHistoryStorage implements HistoryStorage {
    private static Connection conn = null;

    public Connection getConnection() {
        if(conn==null) {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection("jdbc:mysql://localhost/hand_history","poker","poker");
            } catch(Exception e) {
                // TODO: throw local exception
                e.printStackTrace();
            }
        }
        return conn;
    }

    public int storeHoldEmDeal(HoldEmDeal deal) {
        String cols = "site, game_number, game_type, table_name, sb, bb, description, pot, rake, timestamp";
        String values = "'" + deal.getSite() + "'";
        values += ",'" + deal.getGameNumber() + "'";
        values += ",'" + deal.getGameType() + "'";
        values += ",'" + deal.getTableName() + "'";
        values += "," + deal.getSmallBlind();
        values += "," + deal.getBigBlind();
        values += ",'" + deal.getDescription().replace("'", "") + "'";
        values += "," + deal.getPot();
        values += "," + deal.getRake();
        values += ",'" + deal.getTimestamp() + "'";
        String command = "INSERT INTO holdem_deal (" + cols + ") VALUES (" + values + ");";
        return runInsert(command);
    }

    public int storeHoldEmHand(HoldEmHand hand) {
        String cols = "deal_id, player_name, seat_num, button, sb, bb, vpip, pre_flop_raise, num_bets, num_raises, num_calls, winner, profit";
        String values = "" + hand.getDealId();
        values += ",'" + hand.getPlayerName().replace("'", "") + "'";
        values += "," + hand.getSeatNumber();
        values += "," + boolToInt(hand.isButton());
        values += "," + boolToInt(hand.isSmallBlind());
        values += "," + boolToInt(hand.isBigBlind());
        values += "," + boolToInt(hand.isVpip());
        values += "," + boolToInt(hand.isPreFlopRaise());
        values += "," + hand.getNumberOfBets();
        values += "," + hand.getNumberOfRaises();
        values += "," + hand.getNumberOfCalls();
        values += "," + boolToInt(hand.isWinner());
        values += "," + hand.getProfit();
        String command = "INSERT INTO holdem_hand (" + cols + ") VALUES (" + values + ");";
        return runInsert(command);
    }

    public int storeHandAction(HandAction action) {
        String cols = "deal_id, hand_id, round, action, value, current_pot_size, current_call_amount";
        String values = "" + action.getHoldEmHand().getDealId();
        values += "," + action.getHoldEmHand().getId();
        values += ",'" + action.getRound() + "'";
        values += ",'" + action.getAction() + "'";
        values += "," + action.getValue();
        values += "," + action.getCurrentPotSize();
        values += "," + action.getCurrentCallAmount();
        String command = "INSERT INTO hand_action (" + cols + ") VALUES (" + values + ");";
        return runInsert(command);
    }

    private int boolToInt(boolean b) {
        if(b) return 1;
        return 0;
    }

    private int runInsert(String command) {
        int id = -1;
        try {
            Statement s = getConnection().createStatement();
            int count = s.executeUpdate(command, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = s.getGeneratedKeys();
            if(rs.next())
                id = rs.getInt(1);
            s.close();
        } catch (Exception e) {
            System.out.println("MYSQL ERROR: " + command);
            //e.printStackTrace();
        }
        return id;
    }
}
