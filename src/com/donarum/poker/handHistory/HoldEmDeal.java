/*
 * $Id: HoldEmDeal.java 17 2010-06-14 03:33:46Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker.handHistory;

import com.donarum.poker.handHistory.HandAction.Action;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class HoldEmDeal {
    private int id = -1;
    private String site = "unknown";
    private String gameNumber = "";
    private String gameType = "";
    private String description = "";
    private String tableName = "";
    private int smallBlind = 0;
    private int bigBlind = 0;
    private int pot = 0;
    private int rake = 0;
    private String timestamp = "";
    private Vector<HoldEmHand> hands = new Vector<HoldEmHand>();
    private Vector<HandAction> actions = new Vector<HandAction>();

    /**
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @return the gameNumber
     */
    public String getGameNumber() {
        return gameNumber;
    }

    /**
     * @param gameNumber the gameNumber to set
     */
    public void setGameNumber(String gameNumber) {
        this.gameNumber = gameNumber;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * @return the smallBlind
     */
    public int getSmallBlind() {
        return smallBlind;
    }

    /**
     * @param smallBlind the smallBlind to set
     */
    public void setSmallBlind(int smallBlind) {
        this.smallBlind = smallBlind;
    }

    /**
     * @return the bigBlind
     */
    public int getBigBlind() {
        return bigBlind;
    }

    /**
     * @param bigBlind the bigBlind to set
     */
    public void setBigBlind(int bigBlind) {
        this.bigBlind = bigBlind;
    }

    /**
     * @return the hands
     */
    public Vector<HoldEmHand> getHands() {
        return hands;
    }

    /**
     * @return the hands
     */
    public void addHand(HoldEmHand hand) {
        hands.add(hand);
    }

    public HoldEmHand getHandByPlayerName(String name) throws HandHistoryException {
        for(HoldEmHand hand : hands) {
            if(name.equals(hand.getPlayerName()))
                return hand;
        }
        throw new HandHistoryException("Player " + name + " not found");
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the pot
     */
    public int getPot() {
        return pot;
    }

    /**
     * @param pot the pot to set
     */
    public void setPot(int pot) {
        this.pot = pot;
    }

    /**
     * @return the rake
     */
    public int getRake() {
        return rake;
    }

    /**
     * @param rake the rake to set
     */
    public void setRake(int rake) {
        this.rake = rake;
    }

    /**
     * @return the gameType
     */
    public String getGameType() {
        return gameType;
    }

    /**
     * @param gameType the gameType to set
     */
    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp In the format 2007-05-23 09:15:28
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the HandActions
     */
    public Vector<HandAction> getActions() {
        return actions;
    }

    /**
     * Calculates the current bet size.  This is the amount that the current
     * aggressor has put into the pot this round.
     * @return
     */
    public int getCurrentBetSize() {
        int out = 0;
        for(int i=this.getActions().size()-1; i>=0; i--) {
            HandAction a = this.getActions().get(i);
            if(a.getRound()!=this.getActions().lastElement().getRound())
                return out;
            if(a.getAction()==Action.BET || a.getAction()==Action.RAISE || a.getAction()==Action.POST_BB)
                return out + a.getValue();
        }
        return out;
    }

    /**
     * Calculates the current pot size.  Not meant to be used for final tally.
     * @return
     */
    public int getCurrentPotSize() {
        int out = 0;
        for(HoldEmHand hand : this.getHands()) {
            out += hand.getProfit();
        }
        return out;
    }

    public String toString() {
        String out = "";
        out += "id: " + id + "\n";
        out += "site: " + site + "\n";
        out += "gameNumber: " + gameNumber + "\n";
        out += "gameType: " + gameType + "\n";
        out += "description: " + description + "\n";
        out += "tableName: " + tableName + "\n";
        out += "smallBlind: " + smallBlind + "\n";
        out += "bigBlind: " + bigBlind + "\n";
        out += "pot: " + pot + "\n";
        out += "rake: " + rake + "\n";
        out += "timestamp: " + timestamp + "\n";
        for(HoldEmHand hand : hands) {
            out += "---\n";
            out += hand.toString();
        }
        return out;
    }

}
