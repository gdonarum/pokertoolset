/*
 * $Id: Hand.java 3 2010-06-02 17:27:51Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.util.Vector;

/**
 *
 * @author greg
 */
public abstract class Hand implements Comparable {

    protected Vector<Card> cards = new Vector<Card>();

//    public abstract Hand() {
//    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public int getNumberOfCards() {
        return cards.size();
    }

    public Card getCard(int index) {
        return cards.get(index);
    }

    public abstract int compareTo(Object t);

    public String toString() {
        String out = "";
        for (Card card : cards) {
            out += card.getShortName();
        }
        return out;
    }
}

