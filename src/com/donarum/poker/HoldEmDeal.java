/*
 * $Id: HoldEmDeal.java 7 2010-06-07 02:57:30Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class HoldEmDeal extends Deal {
    private String communityCards = "";
    protected Deck deck = new Deck();

    public HoldEmDeal() {
        super();
        deck.shuffle();
    }

    public void addPlayer(Player player, int seatNumber) {
        super.addPlayer(player, seatNumber);
        initiateSeatHand(this.getNumSeats()-1,new HoldEmHand());
    }

    /**
     * Used for simulations and game play.  Deals cards.
     */
    public void dealHoleCards() {
        for(int i=0; i<2; i++) {
            for(int p=0; p<seats.size(); p++) {
                seats.get(p).getHand().addCard(deck.deal());
            }
        }
    }

    public void dealFlop() {
        // burn one
        deck.deal();
        // turn three
        communityCards = "[";
        for(int i=0; i<3; i++) {
            Card card = deck.deal();
            for(int p=0; p<seats.size(); p++) {
                seats.get(p).getHand().addCard(card);
            }
            communityCards+=card.getShortName();
        }
        communityCards+="]";
    }

    public void dealTurn() {
        // burn one
        deck.deal();
        // turn one
        communityCards+="[";
        Card card = deck.deal();
        for(int p=0; p<seats.size(); p++) {
            seats.get(p).getHand().addCard(card);
        }
        communityCards+=card.getShortName();
        communityCards+="]";
    }

    public void dealRiver() {
        // burn one
        deck.deal();
        // turn one
        communityCards+="[";
        Card card = deck.deal();
        for(int p=0; p<seats.size(); p++) {
            seats.get(p).getHand().addCard(card);
        }
        communityCards+=card.getShortName();
        communityCards+="]";
    }

    public String getCommunityCards() {
        return this.communityCards;
    }
    
    public Vector<Seat> getWinners() {
        Vector<Seat> winners = new Vector<Seat>();
        for(Seat seat : this.seats) {
            if(winners.size()==0) {
                winners.add(seat);
            } else {
                switch(winners.get(0).getHand().compareTo(seat.getHand())) {
                    case -1:
                        winners.removeAllElements();
                        winners.add(seat);
                        break;
                    case 0:
                        winners.add(seat);
                }
            }
        }
        return winners;
    }

    public String toString() {
        String out = "";
        out += "Board: " + getCommunityCards() + "\n";
        for(Seat seat : this.seats) {
            out += seat.getPlayer().getName() + ": " + ((HoldEmHand)seat.getHand()).getHoleCards() + " " + ((HoldEmHand)seat.getHand()).getVerboseRank() + "\n";
        }
        for(Seat seat : this.getWinners()) {
            out += "Winner: " + seat.getPlayer().getName() + ": " + ((HoldEmHand)seat.getHand()).getVerboseRank() + "\n";
        }
        return out;
    }

    private void insertRow(Connection conn, String startingHand, int numSeats, boolean isConnected, boolean isPaired, boolean isSuited, int rank, boolean isWinner) throws SQLException {
        Statement s = conn.createStatement();
        int count;
        int isC = (isConnected ? 1 : 0);
        count = s.executeUpdate(
                "INSERT INTO holdem_hand (hand, seats, connected, paired, suited, rank, winner)"
                + " VALUES"
                + "('" + startingHand + "'"
                + "," + numSeats
                + "," + (isConnected ? 1 : 0)
                + "," + (isPaired ? 1 : 0)
                + "," + (isSuited ? 1 : 0)
                + "," + rank
                + "," + (isWinner ? 1 : 0) + ")");
        s.close();
    }


    public static final void main(String[] args) {
        int NUM_SEATS = 6;
        DatabaseConnection db = new DatabaseConnection();
        Connection conn = db.getConnection();
        Player[] players = new Player[NUM_SEATS];
        for(int i=0; i<players.length; i++) {
            players[i] = new Player("Player " + (i+1));
        }
        int connected=0;
        int paired=0;
        int suited=0;
        int suitedAndConnected = 0;
        int totalconnected=0;
        int totalpaired=0;
        int totalsuited=0;
        int totalsuitedAndConnected = 0;
        for(int i=0; i<100000; i++) {
            HoldEmDeal deal = new HoldEmDeal();
            for(int p=0; p<players.length; p++) {
                deal.addPlayer(players[p],p);
            }
            deal.dealHoleCards();
            deal.dealFlop();
            deal.dealTurn();
            deal.dealRiver();
            for(Seat seat : deal.seats) {
                if(((HoldEmHand)seat.getHand()).isConnected()) totalconnected++;
                if(((HoldEmHand)seat.getHand()).isPaired()) totalpaired++;
                if(((HoldEmHand)seat.getHand()).isSuited()) totalsuited++;
                if(((HoldEmHand)seat.getHand()).isConnected()
                        && ((HoldEmHand)seat.getHand()).isSuited()) totalsuitedAndConnected++;
                try {
                    deal.insertRow(conn,
                            ((HoldEmHand)seat.getHand()).getHoleCards().getClassification(),
                            NUM_SEATS,
                            ((HoldEmHand)seat.getHand()).isConnected(),
                            ((HoldEmHand)seat.getHand()).isPaired(),
                            ((HoldEmHand)seat.getHand()).isSuited(),
                            ((HoldEmHand)seat.getHand()).getRank(),
                            deal.getWinners().contains(seat));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //System.out.println(deal.toString());

//            System.out.print(((HoldEmHand)deal.getWinners().get(0).getHand()).isConnected());
//            System.out.print(",");
//            System.out.print(((HoldEmHand)deal.getWinners().get(0).getHand()).isPaired());
//            System.out.print(",");
//            System.out.print(((HoldEmHand)deal.getWinners().get(0).getHand()).isSuited());
//            System.out.print(",");
//            System.out.println(((HoldEmHand)deal.getWinners().get(0).getHand()).getHoleCards());

            if(((HoldEmHand)deal.getWinners().get(0).getHand()).isConnected()) connected++;
            if(((HoldEmHand)deal.getWinners().get(0).getHand()).isPaired()) paired++;
            if(((HoldEmHand)deal.getWinners().get(0).getHand()).isSuited()) suited++;
            if(((HoldEmHand)deal.getWinners().get(0).getHand()).isConnected()
                    && ((HoldEmHand)deal.getWinners().get(0).getHand()).isSuited()) suitedAndConnected++;
            //System.out.println("-------------------------------");
        }
        System.out.println("connected: " + connected + " / " + totalconnected + " = " + ((double)connected/(double)totalconnected));
        System.out.println("suited: " + suited + " / " + totalsuited + " = " + ((double)suited/(double)totalsuited));
        System.out.println("suited&connected: " + suitedAndConnected + " / " + totalsuitedAndConnected + " = " + ((double)suitedAndConnected/(double)totalsuitedAndConnected));
        System.out.println("paired: " + paired + " / " + totalpaired + " = " + ((double)paired/(double)totalpaired));

    }
}

