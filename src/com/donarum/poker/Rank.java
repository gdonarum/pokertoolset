/*
 * $Id: Rank.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 * Enumeration for card ranks.  The values are indexed starting at 1.  Also
 * included are prime numbers.  The primes make hand evaluation much easier.
 * See Cactus Kev's evaluation algorithms at
 * http://www.suffecool.net/poker/evaluator.html
 * I only use the primes for finding five card straights.  I find the rest too
 * complex for this library because they require lookup tables.
 * @author greg
 */
public enum Rank {
    DEUCE("2","Deuce",2),
    TRE("3","Three",3),
    FOUR("4","Four",5),
    FIVE("5","Five",7),
    SIX("6","Six",11),
    SEVEN("7","Seven",13),
    EIGHT("8","Eight",17),
    NINE("9","Nine",19),
    TEN("T","Ten",23),
    JACK("J","Jack",29),
    QUEEN("Q","Queen",31),
    KING("K","King",37),
    ACE("A","Ace",41);

    private final String shortName;
    private final String longName;
    private final int prime;

    Rank(String shortName, String longName, int prime) {
        this.shortName = shortName;
        this.longName = longName;
        this.prime = prime;
    }

    public String getShortName()   { return shortName; }
    public String getLongName() { return longName; }
    public int getPrime() { return prime; }

    /**
     * Compare the given text to the name or abbreviation ignoring case and
     * return the match.
     * @param text Name or abbreviation of a rank.
     * @return One of the Ranks
     * @throws CardException if the value does not match anything.
     */
    public static Rank valueOfRank(String text) throws PokerException {
        for(int i=0; i<Rank.values().length; i++) {
            if(text.equalsIgnoreCase(Rank.values()[i].getShortName()) || text.equalsIgnoreCase(Rank.values()[i].getLongName()))
                return Rank.values()[i];
        }
        throw new PokerException("Invalid rank");
    }

    public static void main(String[] args) {
        System.out.println(Rank.KING);
        try {
            System.out.println(Rank.valueOfRank("Queen"));
            System.out.println(Rank.valueOfRank("4"));
            System.out.println(Rank.valueOfRank("t"));
            System.out.println(Rank.valueOfRank("aCE"));
            System.out.println(Rank.valueOfRank("bb"));
        } catch(PokerException ce) {
            System.out.println(ce.getMessage());
        }
    }

}

