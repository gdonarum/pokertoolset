/*
 * $Id: OmahaDeal.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class OmahaDeal extends Deal {
    private String communityCards = "";
    protected Deck deck = new Deck();

    public OmahaDeal() {
        super();
        deck.shuffle();
    }

    public void addPlayer(Player player, int seatNumber) {
        super.addPlayer(player, seatNumber);
        initiateSeatHand(this.getNumSeats()-1,new OmahaHand());
    }

    public void dealHoleCards() {
        for(int i=0; i<4; i++) {
            for(int p=0; p<seats.size(); p++) {
                seats.get(p).getHand().addCard(deck.deal());
            }
        }
    }

    public void dealFlop() {
        // burn one
        deck.deal();
        // turn three
        communityCards = "[";
        for(int i=0; i<3; i++) {
            Card card = deck.deal();
            for(int p=0; p<seats.size(); p++) {
                seats.get(p).getHand().addCard(card);
            }
            communityCards+=card.getShortName();
        }
        communityCards+="]";
    }

    public void dealTurn() {
        // burn one
        deck.deal();
        // turn one
        communityCards += "[";
        Card card = deck.deal();
        for(int p=0; p<seats.size(); p++) {
            seats.get(p).getHand().addCard(card);
        }
        communityCards+=card.getShortName();
        communityCards+="]";
    }

    public void dealRiver() {
        // burn one
        deck.deal();
        // turn one
        communityCards += "[";
        Card card = deck.deal();
        for(int p=0; p<seats.size(); p++) {
            seats.get(p).getHand().addCard(card);
        }
        communityCards+=card.getShortName();
        communityCards+="]";
    }

    public String getCommunityCards() {
        return this.communityCards;
    }

    public Vector<Seat> getWinners() {
        Vector<Seat> winners = new Vector<Seat>();
        for(Seat seat : this.seats) {
            if(winners.size()==0) {
                winners.add(seat);
            } else {
                switch(winners.get(0).getHand().compareTo(seat.getHand())) {
                    case -1:
                        winners.removeAllElements();
                        winners.add(seat);
                        break;
                    case 0:
                        winners.add(seat);
                }
            }
        }
        return winners;
    }

    public String toString() {
        String out = "";
        out += "Board: " + getCommunityCards() + "\n";
        for(Seat seat : this.seats) {
            out += seat.getPlayer().getName() + ": " + ((OmahaHand)seat.getHand()).getHoleCards() + " " + ((OmahaHand)seat.getHand()).getVerboseRank() + "\n";
        }
        for(Seat seat : this.getWinners()) {
            out += "Winner: " + seat.getPlayer().getName() + ": " + ((OmahaHand)seat.getHand()).getVerboseRank() + "\n";
        }
        return out;
    }

    private void insertRow(Connection conn, String startingHand, int numSeats, boolean isConnected, boolean isSinglePaired, boolean isDoublePaired, boolean isSingleSuited, boolean isDoubleSuited, int rank, boolean isWinner) throws SQLException {
        Statement s = conn.createStatement();
        int count;
        int isC = (isConnected ? 1 : 0);
        count = s.executeUpdate(
                "INSERT INTO omaha_hand (hand, seats, connected, single_paired, double_paired, single_suited, double_suited, rank, winner)"
                + " VALUES"
                + "('" + startingHand + "'"
                + "," + numSeats
                + "," + (isConnected ? 1 : 0)
                + "," + (isSinglePaired ? 1 : 0)
                + "," + (isDoublePaired ? 1 : 0)
                + "," + (isSingleSuited ? 1 : 0)
                + "," + (isDoubleSuited ? 1 : 0)
                + "," + rank
                + "," + (isWinner ? 1 : 0) + ")");
        s.close();
    }


    public static final void main(String[] args) {
        int NUM_SEATS = 6;
        DatabaseConnection db = new DatabaseConnection();
        Connection conn = db.getConnection();
        Player[] players = new Player[NUM_SEATS];
        for(int i=0; i<players.length; i++) {
            players[i] = new Player("Player " + (i+1));
        }
        int connected=0;
        int singlepaired=0;
        int doublepaired=0;
        int singlesuited=0;
        int doublesuited=0;
        int totalconnected=0;
        int totalsinglepaired=0;
        int totaldoublepaired=0;
        int totalsinglesuited=0;
        int totaldoublesuited=0;
        for(int i=0; i<10000; i++) {
            OmahaDeal deal = new OmahaDeal();
            for(int p=0; p<players.length; p++) {
                deal.addPlayer(players[p],p);
            }
            deal.dealHoleCards();
            deal.dealFlop();
            deal.dealTurn();
            deal.dealRiver();
            for(Seat seat : deal.seats) {
                if(((OmahaHand)seat.getHand()).isConnected()) totalconnected++;
                if(((OmahaHand)seat.getHand()).isSinglePaired()) totalsinglepaired++;
                if(((OmahaHand)seat.getHand()).isDoublePaired()) totaldoublepaired++;
                if(((OmahaHand)seat.getHand()).isSingleSuited()) totalsinglesuited++;
                if(((OmahaHand)seat.getHand()).isDoubleSuited()) totaldoublesuited++;
                try {
                    deal.insertRow(conn,
                            ((OmahaHand)seat.getHand()).getClassification(),
                            NUM_SEATS,
                            ((OmahaHand)seat.getHand()).isConnected(),
                            ((OmahaHand)seat.getHand()).isSinglePaired(),
                            ((OmahaHand)seat.getHand()).isDoublePaired(),
                            ((OmahaHand)seat.getHand()).isSingleSuited(),
                            ((OmahaHand)seat.getHand()).isDoubleSuited(),
                            ((OmahaHand)seat.getHand()).getRank(),
                            deal.getWinners().contains(seat));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            System.out.println(deal.toString());

//            System.out.print(((OmahaHand)deal.getWinners().get(0).getHand()).isConnected());
//            System.out.print(",");
//            System.out.print(((OmahaHand)deal.getWinners().get(0).getHand()).isPaired());
//            System.out.print(",");
//            System.out.print(((OmahaHand)deal.getWinners().get(0).getHand()).isSuited());
//            System.out.print(",");
//            System.out.println(((OmahaHand)deal.getWinners().get(0).getHand()).getHoleCards());

            if(((OmahaHand)deal.getWinners().get(0).getHand()).isConnected()) connected++;
            if(((OmahaHand)deal.getWinners().get(0).getHand()).isSinglePaired()) singlepaired++;
            if(((OmahaHand)deal.getWinners().get(0).getHand()).isDoublePaired()) doublepaired++;
            if(((OmahaHand)deal.getWinners().get(0).getHand()).isSingleSuited()) singlesuited++;
            if(((OmahaHand)deal.getWinners().get(0).getHand()).isDoubleSuited()) doublesuited++;
            //System.out.println("-------------------------------");
        }
//        System.out.println("connected: " + connected + " / " + totalconnected + " = " + ((double)connected/(double)totalconnected));
//        System.out.println("single suited: " + singlesuited + " / " + totalsinglesuited + " = " + ((double)singlesuited/(double)totalsinglesuited));
//        System.out.println("double suited: " + doublesuited + " / " + totaldoublesuited + " = " + ((double)doublesuited/(double)totaldoublesuited));
//        System.out.println("single paired: " + singlepaired + " / " + totalsinglepaired + " = " + ((double)singlepaired/(double)totalsinglepaired));
//        System.out.println("double paired: " + doublepaired + " / " + totaldoublepaired + " = " + ((double)doublepaired/(double)totaldoublepaired));

    }
}

