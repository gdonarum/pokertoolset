/*
 * $Id: Seat.java 6 2010-06-03 03:38:56Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 *
 * @author greg
 */
public class Seat {

    private Player player;
    private Hand hand;
    private int seatNumber = -1;

    public Seat(Player player, int seatNumber) {
        this.player = player;
        this.seatNumber = seatNumber;
    }

    public void initiateHand(Hand hand) {
        this.hand = hand;
    }

    public Hand getHand() {
        return this.hand;
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getSeatNumber() {
        return this.seatNumber;
    }

    public String toString() {
        //String out = player.getName() + "," + hand.toString() + "," + hand.getBestFiveCardHand().toString();
        String out = player.getName() + "," + hand.toString();
        return out;
    }
}

