/*
 * $Id: FiveCardHand.java 5 2010-06-03 00:54:59Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.lang.Comparable;
import java.util.Arrays;

/**
 * Five card hand evaluator.  This is mostly my work except for the straight
 * calculation.  For more info on using primes see Cactus Kev's evaluation
 * algorithms at http://www.suffecool.net/poker/evaluator.html
 * I only use Kev's ideas for finding five card straights.  I find the rest too
 * complex for this library because they require lookup tables.
 * @author  Greg
 */
public class FiveCardHand extends Hand { // implements Comparable {
    
    public static final int RANK_ROYAL_FLUSH = 9;
    public static final int RANK_STRAIGHT_FLUSH = 8;
    public static final int RANK_FOUR_OF_A_KIND = 7;
    public static final int RANK_FULL_HOUSE = 6;
    public static final int RANK_FLUSH = 5;
    public static final int RANK_STRAIGHT = 4;
    public static final int RANK_THREE_OF_A_KIND = 3;
    public static final int RANK_TWO_PAIR = 2;
    public static final int RANK_PAIR = 1;
    public static final int RANK_HIGH_CARD = 0;

    private static final int[] STRAIGHT_PRIMES = {8610,2310,15015,85085,323323,1062347,2800733,6678671,14535931,31367009};
    
    public static final String[] ranks = {"No Pair"
                                        , "Pair"
                                        ,"Two Pair"
                                        ,"Three of a Kind"
                                        ,"Straight"
                                        ,"Flush"
                                        ,"Full House"
                                        ,"Four of a Kind"
                                        ,"Straight Flush"
                                        ,"Royal Flush"};
    
    Card[] fiveCards = new Card[5];
    Card[] orderedFiveCards = new Card[5];
     
    /** Creates a new instance of FiveCardHand */
    public FiveCardHand() {
    }
    
    protected void setFiveCards(Card[] fiveCards) {
        if(fiveCards.length==5) {
            for(int i=0; i<fiveCards.length; i++) {
                this.fiveCards[i] = fiveCards[i];
                this.orderedFiveCards[i] = fiveCards[i];
            }
            orderCardsByRank(orderedFiveCards);
        }
    }

    public Card[] getFiveCards() {
        return fiveCards;
    }
    
    private void orderCardsByRank(Card[] c) {
        // larger cards first
        Arrays.sort(c);
        // account for wheel
        if(c[0].getRank()==Rank.ACE
            && c[1].getRank()==Rank.FIVE
            && c[2].getRank()==Rank.FOUR
            && c[3].getRank()==Rank.TRE
            && c[4].getRank()==Rank.DEUCE) {
            Card temp = c[0];
            c[0]=c[1];
            c[1]=c[2];
            c[2]=c[3];
            c[3]=c[4];
            c[4]=temp;
        }
        orderPairs(orderedFiveCards);
    }
     
    private boolean isRoyalFlush() {
        if(orderedFiveCards[0].getRank() != Rank.ACE)
            return false;
        return isStraightFlush();
    }
     
    private boolean isStraightFlush() {
        return (isFlush() && isStraight());
    }
    
    private boolean isFourOfAKind() {
        // aaaab or abbbb
        return ((orderedFiveCards[0].getRank()==orderedFiveCards[3].getRank()) || (orderedFiveCards[1].getRank()==orderedFiveCards[4].getRank()));
    }
    
    private boolean isFullHouse() {
        // aabbb or aaabb
        return ( 
            ((orderedFiveCards[0].getRank()==orderedFiveCards[1].getRank()) && (orderedFiveCards[2].getRank()==orderedFiveCards[4].getRank()))
            || ((orderedFiveCards[0].getRank()==orderedFiveCards[2].getRank()) && (orderedFiveCards[3].getRank()==orderedFiveCards[4].getRank()))
            );
    }
    
    private boolean isFlush() {
        for (int i=1; i < fiveCards.length; i++) {
            if (fiveCards[i-1].getSuit() != fiveCards[i].getSuit())
                return false;
        }
        return true;
    }

    /**
     * Using Cactus Kev's prime number straight evaluation idea.
     * For more info see:
     * http://www.suffecool.net/poker/evaluator.html
     * @return
     */
    private boolean isStraight() {
        int prime = 1;
        // calculate the combined prime of the hand
        for(Card c : orderedFiveCards)
            prime *= c.getRank().getPrime();
        // see if it's a straight prime
        for(int p : STRAIGHT_PRIMES) {
            if(prime==p)
                return true;
        }
        return false;
    }
    
    private boolean isThreeOfAKind() {
        if(isFullHouse()) return false;
        // aaabc or abbbc or abccc
        return ((orderedFiveCards[0].getRank()==orderedFiveCards[2].getRank())
            || (orderedFiveCards[1].getRank()==orderedFiveCards[3].getRank())
            || (orderedFiveCards[2].getRank()==orderedFiveCards[4].getRank()));
    }
    
    private boolean isTwoPair() {
        if(isFullHouse()) return false;
        return ( 
            ((orderedFiveCards[0].getRank()==orderedFiveCards[1].getRank()) && (orderedFiveCards[2].getRank()==orderedFiveCards[3].getRank()))
            || ((orderedFiveCards[0].getRank()==orderedFiveCards[1].getRank()) && (orderedFiveCards[3].getRank()==orderedFiveCards[4].getRank()))
            || ((orderedFiveCards[1].getRank()==orderedFiveCards[2].getRank()) && (orderedFiveCards[3].getRank()==orderedFiveCards[4].getRank()))
            );
    }
    
    private boolean isPair() {
        if(isTwoPair()) return false;
        for(int i=1; i < orderedFiveCards.length; i++) {
            if (orderedFiveCards[i-1].getRank() == orderedFiveCards[i].getRank()) {
                return true;
            }
        }
        return false;
    }
    
    public int getRank() {
        if (isRoyalFlush()) return RANK_ROYAL_FLUSH;
        else if (isStraightFlush()) return RANK_STRAIGHT_FLUSH;
        else if (isFourOfAKind()) return RANK_FOUR_OF_A_KIND;
        else if (isFullHouse()) return RANK_FULL_HOUSE;
        else if (isFlush()) return RANK_FLUSH;
        else if (isStraight()) return RANK_STRAIGHT;
        else if (isThreeOfAKind()) return RANK_THREE_OF_A_KIND;
        else if (isTwoPair()) return RANK_TWO_PAIR;
        else if (isPair()) return RANK_PAIR;
        else return RANK_HIGH_CARD;
    }

    public String getVerboseRank() {
        String out = "";
        out += ranks[getRank()];
        switch(getRank()) {
            case RANK_STRAIGHT_FLUSH:
            case RANK_FLUSH:
            case RANK_STRAIGHT:
                out += ": " + this.orderedFiveCards[0].getRank().getLongName() + " high";
                break;
            case RANK_PAIR:
            case RANK_THREE_OF_A_KIND:
            case RANK_FOUR_OF_A_KIND:
                out += ": " + this.orderedFiveCards[0].getRank().getLongName() + "s";
                break;
        }
        return out;
    }
    
    public String toString() {
        String out = "";
        out += ranks[getRank()];
        for (int i=4; i>=0; i--)
            out += "\n\t" + fiveCards[i];
        return out;
    }
    
    public int compareTo(Object o) {
        int a = getRank();
        FiveCardHand handb = (FiveCardHand)o;
        int b = handb.getRank();
        if (a<b) return -1;
        if (a>b) return 1;
        switch(getRank()) {
            case RANK_ROYAL_FLUSH:
                return 0;
            case RANK_STRAIGHT_FLUSH:
            case RANK_STRAIGHT:
                if (orderedFiveCards[0].getRank().ordinal() < handb.getOrderedFiveCards()[0].getRank().ordinal()) return -1;
                if (orderedFiveCards[0].getRank().ordinal() > handb.getOrderedFiveCards()[0].getRank().ordinal()) return 1;
                return 0;
            case RANK_FLUSH:
            case RANK_HIGH_CARD:
                for(int i=0; i<5; i++) {
                    if (orderedFiveCards[i].getRank().ordinal() < handb.getOrderedFiveCards()[i].getRank().ordinal()) return -1;
                    if (orderedFiveCards[i].getRank().ordinal() > handb.getOrderedFiveCards()[i].getRank().ordinal()) return 1;
                }
                return 0;
            case RANK_FOUR_OF_A_KIND:
            case RANK_FULL_HOUSE:
            case RANK_THREE_OF_A_KIND:
            case RANK_TWO_PAIR:
            case RANK_PAIR:
                for(int i=0; i<5; i++) {
                    if (orderedFiveCards[i].getRank().ordinal() < handb.getOrderedFiveCards()[i].getRank().ordinal()) return -1;
                    if (orderedFiveCards[i].getRank().ordinal() > handb.getOrderedFiveCards()[i].getRank().ordinal()) return 1;
                }
                return 0;
        }
        return 0;
    }
    
    public Card[] getOrderedFiveCards() {
        return orderedFiveCards;
    }
    
//    public Card[] getOrderedPairs() {
//        return orderedPairs;
//    }
    
    private void orderPairs(Card[] c) {
        Card temp;
        switch(getRank()) {
            case RANK_FOUR_OF_A_KIND:
                // aaaab --> do nothing

                // abbbb --> bbbba
                if(c[0].getRank()!=c[1].getRank()) {
                    temp = c[4];
                    c[4] = c[0];
                    c[0] = temp;
                }
                break;
            case RANK_FULL_HOUSE:
                // aaabb --> do nothing

                // aabbb --> bbbaa
                if(c[0].getRank()!=c[2].getRank()) {
                    temp = c[4];
                    c[4] = c[0];
                    c[0] = temp;
                    temp = c[3];
                    c[3] = c[1];
                    c[1] = temp;
                }
                break;
            case RANK_PAIR:
                for(int i=4; i>=2; i--) {
                    if(c[i].getRank()==c[i-1].getRank()) {
                        temp=c[i];
                        c[i]=c[i-2];
                        c[i-2]=temp;
                    }
                }
                break;
            case RANK_THREE_OF_A_KIND:
                for(int i=4; i>=3; i--) {
                    if(c[i].getRank()==c[i-1].getRank()) {
                        temp=c[i];
                        c[i]=c[i-3];
                        c[i-3]=temp;
                    }
                }
                break;
            case RANK_TWO_PAIR:
                // aabbc --> do nothing

                if(c[0].getRank()==c[1].getRank() && c[3].getRank()==c[4].getRank()) {
                    // aabcc --> aaccb
                    temp = c[4];
                    c[4] = c[2];
                    c[2] = temp;
                } else if(c[1].getRank()==c[2].getRank() && c[3].getRank()==c[4].getRank()) {
                    // abbcc --> bbcca
                    temp = c[4];
                    c[4]=c[0];
                    c[0]=c[2];
                    c[2]=temp;
                }
                break;
        }
    }

    private void printCardArray(Card[] cards) {
        for(Card c : cards)
            System.out.print(c.getRank().getShortName());
        System.out.println();
    }
     
//    public Object clone() {
//        Card[] copyfiveCards = new Card[fiveCards.length];
//        for(int i=0; i<fiveCards.length; i++) {
//            copyfiveCards[i] = (Card)fiveCards[i].clone();
//        }
//        return new FiveCardHand(copyfiveCards);
//    }
    
    public static final void main(String[] args) {
        Deck deck = new Deck();
//        int count = 1000000;
//        int[] results = new int[10];
//        for(int j=0; j<results.length; j++)
//            results[j]=0;
//        while (count-- > 0) {
//            deck.shuffle();
//            Card[] fiveCards = new Card[5];
//            for (int i=0; i<fiveCards.length; i++) {
//                fiveCards[i] = deck.dealCard();
//            }
//            FiveCardHand hand = new FiveCardHand(fiveCards);
//            results[hand.getRank()]++;
//        }
//        for(int j=0; j<results.length; j++)
//            System.out.println(j + "=" + results[j]);
        
        
        deck.shuffle();
        Card[] fiveCards = new Card[5];
        for (int i=0; i<fiveCards.length; i++) {
            fiveCards[i] = deck.deal();
        }
        FiveCardHand hand = new FiveCardHand();
        hand.setFiveCards(fiveCards);
        Card[] fiveCards2 = new Card[5];
        for (int i=0; i<fiveCards2.length; i++) {
            fiveCards2[i] = deck.deal();
        }
        FiveCardHand hand2 = new FiveCardHand();
        hand2.setFiveCards(fiveCards2);
        
        System.out.println(hand);
        System.out.println(hand.compareTo(hand2));
        System.out.println(hand2);
    }
    
}

