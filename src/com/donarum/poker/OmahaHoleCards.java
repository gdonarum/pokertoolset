/*
 * $Id: OmahaHoleCards.java 16 2010-06-13 04:05:57Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 *
 * @author greg
 */
public class OmahaHoleCards implements HoldEmHoleCards {

    private Card card1;
    private Card card2;
    private Card card3;
    private Card card4;

    public OmahaHoleCards(Card card1, Card card2, Card card3, Card card4) {
        this.card1=card1;
        this.card2=card2;
        this.card3=card3;
        this.card4=card4;
    }

    public String toString() {
        return card1.getShortName() + " " + card2.getShortName() + " " + card3.getShortName() + " " + card4.getShortName();
    }
}
