/*
 * $Id: TexasHoldEmHoleCards.java 16 2010-06-13 04:05:57Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

/**
 *
 * @author greg
 */
public class TexasHoldEmHoleCards implements HoldEmHoleCards {

    private Card card1;
    private Card card2;
    public static enum Range {A,B,C,D,E}

    public TexasHoldEmHoleCards(Card card1, Card card2) {
        this.card1=card1;
        this.card2=card2;
    }


    public String getClassification() {
        String out = "";
        if(card1.getRank().compareTo(card2.getRank())>=0)
            out += card1.getRank().getShortName() + card2.getRank().getShortName();
        else
            out += card2.getRank().getShortName() + card1.getRank().getShortName();
        if(card1.isPaired(card2))
            out += "";
        else if(card1.isSuited(card2))
            out +="s";
        else
            out += "o";
        return out;
    }

    /**
     * http://www.toptexasholdem.com/texas-holdem-strategy-startinghand.php
     * @return
     */
    public Range getRange() {
        if("AA,KK,QQ,AKs".contains(this.getClassification()))
            return Range.A;
        if("JJ,TT,AKo,AQs".contains(this.getClassification()))
            return Range.B;
        if("99,AQo,AJs,KQs,KQo".contains(this.getClassification()))
            return Range.C;
        if("88,77,66,55,44,33,22,KJs,KTs,QJs,QTs,JTs,T9s,98s,87s,76s".contains(this.getClassification()))
            return Range.D;
        if(this.getClassification().startsWith("A") && this.getClassification().endsWith("s"))
            return Range.D;
        return Range.E;
    }

    /**
     * http://en.wikipedia.org/wiki/Chen_formula#Chen_formula
     * Chen is the author of the Chen Formula which was published in Lou Krieger's
     * 2000 book Hold 'em Excellence. The formula ranks Texas hold 'em starting
     * hands by assigning them a numerical value, as follows:
     * 1. Take the high card and score it. A=10,K=8,Q=7,J=6 and 10 to 2 = 1/2 the
     * value of the card. (a 6 is worth 3, for example)
     * 2. If the 2nd card pairs the first, then the value is twice the high card
     * point. The minimum score for a pair is 5 (e.g., pair of deuces is worth 5, not 4).
     * 3. Round up any half points.
     * 4. If the cards are not paired then calculate the gap for the lower card
     * and subtract a gap penalty. The gap is the number of cards required to
     * complete the sequence, for example, a 9 and 6 have a gap of 2, needing an 8 and 7 to complete the sequence:
          * For a gap of 0 subtract 0.
          * For a gap of 1 subtract 1.
          * For a gap of 2 subtract 2.
          * For a gap of 3 subtract 4.
          * For a gap of 4 or more subtract 5 (includes A2,A3,A4, A5).
     * 5. If the cards are of the same suit apply a flush bonus of +2 pts.
     * 6. If the cards have a gap of 0 or 1 and the top card is a J or lower apply a +1 straight bonus.
     * @return
     */
    public int getChenScore() {
        int score = 0;
        Card high = card1;
        Card low = card2;
        if(high.getRank().ordinal()<low.getRank().ordinal()) {
            high = card2;
            low = card1;;
        }
        // 1. score high card
        float fScore=0;
        switch(high.getRank()) {
            case ACE:
                fScore=10;
                break;
            case KING:
                fScore=8;
                break;
            case QUEEN:
                fScore=7;
                break;
            case JACK:
                fScore=6;
                break;
            default:
                fScore = (float)(high.getRank().ordinal()+2)/(float)2;
        }

        // 2. pairs
        boolean isPair = high.getRank()==low.getRank();
        if(isPair) {
            fScore=fScore*2;
            if(fScore==3) fScore=4;
            else if(fScore<5) fScore=5;
        }

        // 3. round up
        score = Math.round(fScore);

        // 4. gap
        int gap = high.getRank().ordinal()-low.getRank().ordinal()-1;
        if(gap<0) gap=0;
        if(gap>5) gap=5;
        score=score-gap;

        // 5. flush bonus
        if(high.getSuit()==low.getSuit())
            score+=2;

        // 6. If the cards have a gap of 0 or 1 and the top card is a J or lower apply a +1 straight bonus.
        if(gap<2 && !isPair) {
            if(high.getRank().ordinal()<10)
                score++;
        }

        return score;
    }

    public String toString() {
        return card1.getShortName() + " " + card2.getShortName();
    }
}
