/*
 * $Id: OmahaHand.java 3 2010-06-02 17:27:51Z gregdonarum $
 *
 * Copyright 2010 Gregory Donarum
 *
 * This file is part of the Poker Analysis Toolset.
 *
 * The Poker Analysis Toolset is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The Poker Analysis Toolset is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the Poker Analysis Toolset.
 * If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.donarum.poker;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

/**
 *
 * @author greg
 */
public class OmahaHand extends FiveCardHand {

    private String holeCards = "";
    private static int NUM_HOLE_CARDS = 4;

    public void addCard(Card card) {
        super.addCard(card);
        if(cards.size()>=7)
            setBestFiveCardHand();
        if(cards.size()<=NUM_HOLE_CARDS)
            holeCards+=card.getShortName();
    }
    private void setBestFiveCardHand() {
        Hand hand = setBestFiveCardHand(null,this);
    }

    private Hand setBestFiveCardHand(Hand bestHand, Hand checkHand) {
        if(checkHand.getNumberOfCards()==5) {
            if(bestHand==null || (bestHand.compareTo(checkHand)<0)) {
                bestHand = checkHand;
                Card[] list = new Card[5];
                for(int i=0; i<list.length; i++)
                    list[i] = bestHand.getCard(i);
                this.setFiveCards(list);
            }
        } else if (checkHand.getNumberOfCards()==7) {
            // pair up the hole cards
            for(int i=0; i<NUM_HOLE_CARDS; i++) {
                for(int j=i+1; j<NUM_HOLE_CARDS; j++) {
                    HoldEmHand newHand = new HoldEmHand();
                    newHand.addCard(checkHand.getCard(i));
                    newHand.addCard(checkHand.getCard(j));
                    newHand.addCard(checkHand.getCard(4));
                    newHand.addCard(checkHand.getCard(5));
                    newHand.addCard(checkHand.getCard(6));
                    bestHand = setBestFiveCardHand(bestHand, newHand);
                }
            }
        } else if (checkHand.getNumberOfCards()>7) {
            // remove one
            for(int i=0; i<checkHand.getNumberOfCards(); i++) {
                OmahaHand newHand = new OmahaHand();
                for(int c=0; c<checkHand.getNumberOfCards(); c++) {
                    if(c!=i)
                        newHand.addCard(checkHand.getCard(c));
                }
                bestHand = setBestFiveCardHand(bestHand, newHand);
            }
        }
        return bestHand;
    }

    public int scoreSuited() {
        int score=0;
        if (this.cards.size() >= NUM_HOLE_CARDS) {
            for(int i=0; i<NUM_HOLE_CARDS; i++) {
                for(int j=i+1; j<NUM_HOLE_CARDS; j++) {
                    if(this.cards.get(i).isSuited(this.cards.get(j))) score++;
                }
            }
        }
        return score;
    }

    public int scorePaired() {
        int score=0;
        if (this.cards.size() >= NUM_HOLE_CARDS) {
            for(int i=0; i<NUM_HOLE_CARDS; i++) {
                for(int j=i+1; j<NUM_HOLE_CARDS; j++) {
                    if(this.cards.get(i).isPaired(this.cards.get(j))) score++;
                }
            }
        }
        return score;
    }

    public int scoreConnected() {
        int score=0;
        if (this.cards.size() >= NUM_HOLE_CARDS) {
            for(int i=0; i<NUM_HOLE_CARDS; i++) {
                for(int j=i+1; j<NUM_HOLE_CARDS; j++) {
                    if(this.cards.get(i).isConnected(this.cards.get(j))) score++;
                }
            }
        }
        return score;
    }
    
    public boolean isSingleSuited() {
        return this.scoreSuited()==1;
    }

    public boolean isDoubleSuited() {
        return this.scoreSuited()==2;
    }

    public boolean isConnected() {
        return this.scoreConnected()==6;
    }

    public boolean isSinglePaired() {
        return this.scorePaired()==1;
    }

    public boolean isDoublePaired() {
        return this.scorePaired()==2;
    }

    public String getHoleCards() {
        return this.holeCards;
    }

    public String getClassification() {
        String out = "";
        Card[] hCards = new Card[NUM_HOLE_CARDS];
        for(int i=0; i<NUM_HOLE_CARDS; i++)
            hCards[i] = cards.get(i);
        Arrays.sort(hCards);
        for(Card c : hCards)
            out+=c.getRank().getShortName();
        return out;
    }

    public String toString() {
        return super.toString() + "," + this.scoreSuited() + "," + this.scoreConnected() + "," + this.scorePaired();
    }
}

