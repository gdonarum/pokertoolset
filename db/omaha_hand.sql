use poker;

CREATE TABLE omaha_hand (
	hand CHAR(4),
	seats TINYINT,
	connected TINYINT,
	single_paired TINYINT,
	double_paired TINYINT,
	single_suited TINYINT,
	double_suited TINYINT,
	rank TINYINT,
	winner TINYINT
);
