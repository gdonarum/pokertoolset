/*
CREATE DATABASE hand_history;
GRANT ALL ON hand_history.* to poker@localhost IDENTIFIED BY 'poker';
*/

use hand_history;

/*DROP TABLE holdem_deal;*/
CREATE TABLE holdem_deal (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	site CHAR(2), /* PS=PokerStars, FT=FullTilt */
	game_number VARCHAR(20),
	game_type VARCHAR(20),
	description VARCHAR(255) NOT NULL,
	table_name VARCHAR(20),
	sb INT,
	bb INT,
	pot INT,
	rake INT,
	timestamp DATETIME
);

/*DROP TABLE holdem_hand;*/
CREATE TABLE holdem_hand (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	deal_id INT NOT NULL,
	player_name VARCHAR(50) NOT NULL,
	seat_num TINYINT NOT NULL,
	button TINYINT(1) NOT NULL DEFAULT 0,
	sb TINYINT(1) NOT NULL DEFAULT 0,
	bb TINYINT(1) NOT NULL DEFAULT 0,
	vpip TINYINT(1) NOT NULL DEFAULT 0,
	num_bets int NOT NULL DEFAULT 0,
	num_raises int NOT NULL DEFAULT 0,
	num_calls int NOT NULL DEFAULT 0,
	pre_flop_raise TINYINT(1) NOT NULL DEFAULT 0,
	winner TINYINT(1) NOT NULL DEFAULT 0,
	profit INT
);

/*DROP TABLE hand_action;*/
CREATE TABLE hand_action (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	deal_id INT NOT NULL,
	hand_id INT NOT NULL,
	round VARCHAR(8) NOT NULL,
	action VARCHAR(21) NOT NULL,
	value int NOT NULL DEFAULT 0,
	current_pot_size int NOT NULL DEFAULT 0,
	current_call_amount int NOT NULL DEFAULT 0
);
