use poker;

CREATE TABLE holdem_hand (
	hand CHAR(3),
	seats TINYINT,
	connected TINYINT,
	paired TINYINT,
	suited TINYINT,
	rank TINYINT,
	winner TINYINT
);
